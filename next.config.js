/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  // basePath: '/lab/boggle/app',
};

module.exports = nextConfig;
