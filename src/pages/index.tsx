import { PlayingSurface } from '@/components/PlayingSurface';
import { GameStateStoreProvider } from '@/state/stores/GameStateStoreProvider';
import styles from '@/styles/Home.module.css';
import Head from 'next/head';

export default function Home() {
  return (
    <>
      <Head>
        <title>Boggle</title>
        <meta name='description' content='Boggle' />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <main className={styles.main}>
        <GameStateStoreProvider>
          <PlayingSurface />
        </GameStateStoreProvider>
      </main>
    </>
  );
}
