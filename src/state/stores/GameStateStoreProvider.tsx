import { createContext, ReactNode, useEffect, useState } from 'react';
import { DiceData } from '../models/DiceData';
import { TileData } from '../models/TileData';
import { WordData } from '../models/WordData';
import BoardService from '../services/BoardService';
import DiceService from '../services/DiceService';
import ScoreService from '../services/ScoreService';

type DefaultContext = {
  tiles: TileData[];
  diceCollection: DiceData[];
  appendLetter: (tileId: string, diceId: string) => void;
  clearWord: () => void;
  submitWord: () => void;
  resetGame: () => void;
  letterQueue: string[];
  currentWord: string[];
  words: WordData[];
  score: number;
  diceMap: Map<string, DiceData>;
};

const defaultContext: DefaultContext = {
  tiles: [],
  diceCollection: [],
  appendLetter: (tileId: string, diceId: string) => {},
  clearWord: () => {},
  submitWord: () => {},
  resetGame: () => {},
  letterQueue: [],
  currentWord: [],
  words: [],
  score: 0,
  diceMap: DiceService.diceMap,
};

export const GameStateStoreContext = createContext(defaultContext);

type Props = {
  children: ReactNode;
};

export const GameStateStoreProvider = ({ children }: Props): JSX.Element => {
  const [tiles, setTiles] = useState<TileData[]>([]);
  const [diceCollection, setDiceCollection] = useState<DiceData[]>([]);
  const [letterQueue, setLetterQueue] = useState<string[]>([]);
  const [currentWord, setCurrentWord] = useState<string[]>([]);
  const [prevTileId, setPrevTileId] = useState<string>('');
  const [words, setWords] = useState<WordData[]>([]);
  const [score, setScore] = useState<number>(0);

  const appendLetter = (tileId: string, diceId: string) => {
    const isFirst = prevTileId === '';
    const isValidAdjacent =
      isFirst || BoardService.isAdjacent(tiles, prevTileId, tileId);
    if (!isValidAdjacent) return;

    const isUsed = letterQueue.find((id) => id === diceId) !== undefined;
    if (isUsed) return;

    setPrevTileId(tileId);

    letterQueue.push(diceId);
    setLetterQueue([...letterQueue]);

    const letter = defaultContext.diceMap.get(diceId)!.face;
    currentWord.push(letter);
    setCurrentWord([...currentWord]);
  };

  const clearWord = () => {
    clearQueues();
  };

  const submitWord = () => {
    const word = currentWord.join('');
    const isNew = words.find((w) => w.value === word) === undefined;
    if (!isNew) {
      clearWord();
      return;
    }

    const points = ScoreService.calculateWord(word);
    const data = new WordData(word, points);
    const list = [...words, data];
    setWords(list);
  };

  const updateScore = () => {
    const score = ScoreService.calculate(words);
    setScore(score);
  };

  const clearQueues = () => {
    setLetterQueue([]);
    setCurrentWord([]);
    setPrevTileId('');
  };

  const resetGame = () => {
    clearQueues();
    setWords([]);
    setScore(0);

    const collection = DiceService.getDefaultDice();
    if (tiles.length === 0) setTiles(BoardService.makeTiles(collection));
    setDiceCollection(DiceService.shuffle(collection));
  };

  useEffect(resetGame, [tiles.length]);
  useEffect(clearQueues, [words]);
  useEffect(updateScore, [words]);

  const api = {
    ...defaultContext,
    tiles,
    diceCollection,
    appendLetter,
    clearWord,
    submitWord,
    resetGame,
    letterQueue,
    currentWord,
    words,
    score,
  };

  return (
    <GameStateStoreContext.Provider value={api}>
      {children}
    </GameStateStoreContext.Provider>
  );
};
