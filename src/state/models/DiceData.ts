export class DiceData {
  id: string;
  sides: string[];
  upIdx: number;

  constructor(sides: string) {
    this.id = `${sides}-${Math.random()}`; // For ease, duplicate condition required the random, but normally I'd use uuid
    this.sides = sides.split('');
    this.upIdx = 0;

    this.shuffle();
  }

  get face() {
    return this.sides[this.upIdx];
  }

  shuffle() {
    const idx = Math.floor(Math.random() * this.sides.length);
    this.upIdx = idx;
  }
}
