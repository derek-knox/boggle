export class TileData {
  id: string;
  diceId: string;

  constructor(i: number, j: number) {
    this.id = `${i}-${j}`;
    this.diceId = '';
  }

  setDiceId(id: string) {
    this.diceId = id;
  }
}
