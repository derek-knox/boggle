export class WordData {
  public value: string;
  public points: number;

  public constructor(value: string, points: number) {
    this.value = value;
    this.points = points;
  }
}
