import { DiceData } from '../models/DiceData';

const DefaultDice: DiceData[] = [
  new DiceData('aaafrs'),
  new DiceData('aaeeee'),
  new DiceData('aafirs'),
  new DiceData('adennn'),
  new DiceData('aeeeem'),
  new DiceData('aeegmu'),
  new DiceData('aegmnn'),
  new DiceData('afirsy'),
  new DiceData('bjkqxz'),
  new DiceData('ccenst'),
  new DiceData('ceiilt'),
  new DiceData('ceilpt'),
  new DiceData('ceipst'),
  new DiceData('ddhnot'),
  new DiceData('dhhlor'),
  new DiceData('dhlnor'),
  new DiceData('dhlnor'),
  new DiceData('eiiitt'),
  new DiceData('emottt'),
  new DiceData('ensssu'),
  new DiceData('fiprsy'),
  new DiceData('gorrvw'),
  new DiceData('iprrry'),
  new DiceData('nootuw'),
  new DiceData('ooottu'),
];

const getDefaultDice = (): DiceData[] => [...DefaultDice];

const diceMap = new Map<string, DiceData>();
getDefaultDice().forEach((dice) => {
  diceMap.set(dice.id, dice);
});

const shuffle = (dice: DiceData[]): DiceData[] => {
  const target: DiceData[] = [];
  let source = dice;

  while (source.length > 0) {
    const idx = Math.random() * source.length;
    const spliceResult = source.splice(idx, 1);
    target.push(spliceResult[0]);
  }

  return target;
};

const DiceService = {
  getDefaultDice,
  diceMap,
  shuffle,
};

export default DiceService;
