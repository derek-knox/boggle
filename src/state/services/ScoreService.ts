import { WordData } from '../models/WordData';

const validWordsUrl =
  'https://raw.githubusercontent.com/raun/Scrabble/master/words.txt';

let validWords: string[];

const init = async () => {
  const response = await fetch(validWordsUrl);
  const data: string = await response.text();
  const arr = data.split('\n');
  validWords = arr;
};
init();

const calculate = (words: WordData[]): number => {
  let score = 0;
  words.forEach(({ points }) => {
    score += points;
  });
  return score;
};

const calculateWord = (word: string): number => {
  const len: number = word.length;

  const isValid = isValidWord(word);
  if (!isValid) return -2;

  if (len >= 3 && len <= 4) return 1;
  if (len === 5) return 2;
  if (len === 6) return 3;
  if (len === 7) return 5;
  if (len >= 8) return 11;

  return -2;
};

const isValidWord = (word: string): boolean => {
  const capsWord = word.toUpperCase();
  return validWords.includes(capsWord);
};

const ScoreService = {
  calculate,
  calculateWord,
};

export default ScoreService;
