import { DiceData } from '../models/DiceData';
import { TileData } from '../models/TileData';

const GRID_SIZE = 5;

const isAdjacent = (
  tiles: TileData[],
  prevTileId: string,
  tileId: string
): boolean => {
  const idxPrev = tiles.findIndex((tile) => tile.id === prevTileId);
  const rowPrev = Math.floor(idxPrev / GRID_SIZE);
  const idxCurr = tiles.findIndex((tile) => tile.id === tileId);
  const rowCurr = Math.floor(idxCurr / GRID_SIZE);
  const isAdjacentRow = Math.abs(rowCurr - rowPrev) === 1;

  const diff = idxCurr - idxPrev;
  if (isAdjacentRow && diff === -GRID_SIZE) return true; // Top
  if (isAdjacentRow && diff === -GRID_SIZE + 1) return true; // Top-Right
  if (!isAdjacentRow && diff === 1) return true; // Right
  if (isAdjacentRow && diff === GRID_SIZE + 1) return true; // Bottom-Right
  if (isAdjacentRow && diff === GRID_SIZE) return true; // Bottom
  if (isAdjacentRow && diff === GRID_SIZE - 1) return true; // Bottom-Left
  if (!isAdjacentRow && diff === -1) return true; // Left
  if (isAdjacentRow && diff === -GRID_SIZE - 1) return true; // Top-Left

  return false;
};

const makeTiles = (diceCollection: DiceData[]): TileData[] => {
  const grid = [];
  for (let i = 0; i < GRID_SIZE; i++) {
    const row: TileData[] = [];
    grid.push(row);
    for (let j = 0; j < GRID_SIZE; j++) {
      row.push(new TileData(i, j));
    }
  }

  const tiles = grid.flat();

  tiles.forEach((tile, idx) => {
    const id = diceCollection[idx].id;
    tile.setDiceId(id);
  });

  return tiles;
};

const BoardService = {
  isAdjacent,
  makeTiles,
};

export default BoardService;
