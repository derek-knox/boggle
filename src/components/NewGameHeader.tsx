import styles from '@/styles/Home.module.css';
import { useCallback, useEffect, useState } from 'react';

type Props = {
  onGameOver: () => void;
  onGameReset: () => void;
};

const DEFAULT_GAME_SECONDS = 180;

export const NewGameHeader = ({ onGameOver, onGameReset }: Props) => {
  const [remainingSeconds, setRemainingSeconds] =
    useState<number>(DEFAULT_GAME_SECONDS);

  const tick = useCallback(() => {
    const time = remainingSeconds - 1;

    if (time < 0) {
      setRemainingSeconds(0);
      onGameOver();
      return;
    }

    const timeout = setTimeout(() => {
      setRemainingSeconds(time);
    }, 1000);

    return () => clearTimeout(timeout);
  }, [onGameOver, remainingSeconds]);

  const onNewGame = () => {
    onGameReset();
    setRemainingSeconds(DEFAULT_GAME_SECONDS);
  };

  useEffect(tick, [tick]);

  const renderTime = () => {
    const minutes = Math.floor(remainingSeconds / 60);
    const seconds = (remainingSeconds % 60).toString().padStart(2, '0');
    return `${minutes}:${seconds}`;
  };

  return (
    <div className={styles.newGameHeader}>
      {remainingSeconds === 0 ? (
        <button className={styles.resetGame} onClick={onNewGame}>
          New Game
        </button>
      ) : (
        renderTime()
      )}
    </div>
  );
};
