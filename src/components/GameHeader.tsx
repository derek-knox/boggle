import styles from '@/styles/Home.module.css';

export const GameHeader = () => {
  return (
    <div className={styles.gameHeader}>
      <span className={styles.tile}>B</span>
      <span className={styles.tile}>O</span>
      <span className={styles.tile}>G</span>
      <span className={styles.tile}>G</span>
      <span className={styles.tile}>L</span>
      <span className={styles.tile}>E</span>
    </div>
  );
};
