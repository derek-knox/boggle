import { GameStateStoreContext } from '@/state/stores/GameStateStoreProvider';
import styles from '@/styles/Home.module.css';
import { useContext } from 'react';
import { CurrentWord } from './CurrentWord';
import { WordsTable } from './WordsTable';

type Props = {
  isGameOver: boolean;
};

export const ScoringPanel = ({ isGameOver }: Props) => {
  const { currentWord, clearWord, submitWord, words, score } = useContext(
    GameStateStoreContext
  );

  const isDisabledSubmit = isGameOver || currentWord.length < 3;
  const isDisabledClear = isGameOver || currentWord.length === 0;

  return (
    <div className={styles.scoringPanel}>
      <CurrentWord queue={currentWord} />
      <div className={styles.actionButtons}>
        <button
          className={styles.submitWord}
          onClick={submitWord}
          disabled={isDisabledSubmit}
        >
          Submit
        </button>
        <button
          className={styles.clearWord}
          onClick={clearWord}
          disabled={isDisabledClear}
        >
          Clear
        </button>
      </div>
      <WordsTable words={words} />
      <p className={styles.score}>
        <span>Score:</span> <span>{score}</span>
      </p>
    </div>
  );
};
