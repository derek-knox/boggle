import styles from '@/styles/Home.module.css';
import { ReactNode } from 'react';

type Props = {
  children: ReactNode;
};

export const LayoutHeader = ({ children }: Props) => {
  return <div className={styles.layoutHeader}>{children}</div>;
};
