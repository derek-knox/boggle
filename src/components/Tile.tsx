import { DiceData } from '@/state/models/DiceData';
import { TileData } from '@/state/models/TileData';
import { GameStateStoreContext } from '@/state/stores/GameStateStoreProvider';
import styles from '@/styles/Home.module.css';
import { useContext } from 'react';

type Props = {
  data: TileData;
  dice: DiceData;
  isGameOver: boolean;
};

export const Tile = ({ data, dice, isGameOver }: Props) => {
  const { appendLetter, letterQueue } = useContext(GameStateStoreContext);

  const onClick = (e: React.MouseEvent) => {
    if (isGameOver) return;

    appendLetter(data.id, dice.id);
  };

  const isSelected = letterQueue.includes(dice.id);
  const classes = `${styles.tile} ${isSelected ? styles.tileSelected : ''} ${
    isGameOver ? styles.tileDisabled : ''
  }`.trim();

  return (
    <div id={dice.id} className={classes} onClick={onClick}>
      {dice.face}
    </div>
  );
};
