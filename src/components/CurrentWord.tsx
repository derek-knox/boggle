import styles from '@/styles/Home.module.css';

type Props = {
  queue: string[];
};

export const CurrentWord = ({ queue }: Props) => {
  return <div className={styles.currentWord}>{queue}</div>;
};
