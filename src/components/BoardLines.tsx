import styles from '@/styles/Home.module.css';
import { useEffect, useRef, useState } from 'react';

type Vector2 = {
  x: number;
  y: number;
};

type Props = {
  ids: string[];
};

export const BoardLines = ({ ids }: Props) => {
  const refCanvas = useRef<null | HTMLCanvasElement>(null);
  const [ctx, setCtx] = useState<null | CanvasRenderingContext2D>(null);
  const [size, setSize] = useState<Vector2>({ x: 0, y: 0 });
  const [positions, setPositions] = useState<Vector2[]>([]);

  const draw = () => {
    if (!refCanvas) return;
    if (!ctx) return;
    if (positions.length < 2) return;

    ctx.lineWidth = 10;
    ctx.strokeStyle = '#ffffff';

    ctx.beginPath();

    const posStart = positions[0];
    ctx.moveTo(posStart.x, posStart.y);
    for (let i = 1; i < positions.length; i++) {
      const { x, y } = positions[i];
      ctx.lineTo(x, y);
    }

    ctx.stroke();
  };

  const updateCtx = () => {
    const canvas = refCanvas.current as HTMLCanvasElement;
    const parent = canvas.parentElement;
    if (!parent) return;

    const dimensions = {
      x: parent.clientWidth,
      y: parent.clientHeight,
    };
    setSize(dimensions);

    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

    setCtx(ctx);
  };

  const updatePositions = () => {
    const result: Vector2[] = ids
      .map((id) => {
        const el = document.getElementById(id);
        if (!el) return;

        const { width } = el.getBoundingClientRect();
        const scaleX = width / el.offsetWidth;
        const delta = 1 - scaleX;
        const value = scaleX !== 1 ? width + Math.round(width * delta) : width;
        const halfSize = Math.floor(value / 2);
        const x = Math.floor(el.offsetLeft + halfSize);
        const y = Math.floor(el.offsetTop + halfSize);
        const pos = { x, y };
        return pos;
      })
      .filter((pos) => pos) as DOMRect[];

    setPositions(result);
  };

  const clear = () => {
    if (!ctx) return;
    if (positions.length > 0) return;

    ctx.clearRect(0, 0, size.x, size.y);
  };

  useEffect(draw, [ctx, positions, size]);
  useEffect(clear, [ctx, positions.length, size]);
  useEffect(updateCtx, [refCanvas]);
  useEffect(updatePositions, [ids]);

  return (
    <canvas
      ref={refCanvas}
      width={size.x}
      height={size.y}
      className={styles.boardLines}
    ></canvas>
  );
};
