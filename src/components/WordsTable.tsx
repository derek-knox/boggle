import { WordData } from '@/state/models/WordData';
import styles from '@/styles/Home.module.css';
import { useEffect, useRef } from 'react';

type Props = {
  words: WordData[];
};

export const WordsTable = ({ words }: Props) => {
  const refScrollList = useRef<null | HTMLDivElement>(null);
  const hasWords = words.length;

  const renderEmpty = () => {
    return (
      <p>
        <em>No words yet</em>
      </p>
    );
  };

  const renderPoints = (points: number) => {
    const isPositive = points > 0;
    const prefix = isPositive ? '+' : '';
    const result = `${prefix}${points}`;
    return <span className={styles.wordPoints}>{result}</span>;
  };

  const renderList = () => {
    return (
      <ol>
        {words.map(({ value, points }) => (
          <li key={value}>
            <span className={styles.wordItem}>
              {value} {renderPoints(points)}
            </span>
          </li>
        ))}
      </ol>
    );
  };

  const scrollIntoView = () => {
    if (!refScrollList.current) return;

    const height = refScrollList.current.scrollHeight;
    refScrollList.current.scroll({
      top: height,
      behavior: 'smooth',
    });
  };

  useEffect(scrollIntoView, [words.length]);

  return (
    <div ref={refScrollList} className={styles.wordsTable}>
      {hasWords ? renderList() : renderEmpty()}
    </div>
  );
};
