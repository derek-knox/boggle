import { DiceData } from '@/state/models/DiceData';
import { TileData } from '@/state/models/TileData';
import styles from '@/styles/Home.module.css';
import { BoardLines } from './BoardLines';
import { Tile } from './Tile';

type Props = {
  tiles: TileData[];
  diceCollection: DiceData[];
  isGameOver: boolean;
  letterQueue: string[];
};

export const Board = ({
  diceCollection,
  tiles,
  isGameOver,
  letterQueue,
}: Props) => {
  return (
    <div className={styles.boardContainer}>
      <BoardLines ids={letterQueue} />
      <div className={styles.board}>
        {tiles.map((tile, idx) => (
          <Tile
            key={tile.id}
            data={tile}
            dice={diceCollection[idx]}
            isGameOver={isGameOver}
          />
        ))}
      </div>
    </div>
  );
};
