import { GameStateStoreContext } from '@/state/stores/GameStateStoreProvider';
import styles from '@/styles/Home.module.css';
import { useCallback, useContext, useState } from 'react';
import { Board } from './Board';
import { GameHeader } from './GameHeader';
import { LayoutHeader } from './LayoutHeader';
import { NewGameHeader } from './NewGameHeader';
import { ScoringPanel } from './ScoringPanel';

export const PlayingSurface = () => {
  const { tiles, diceCollection, letterQueue, resetGame } = useContext(
    GameStateStoreContext
  );
  const [isGameOver, setIsGameOver] = useState<boolean>(false);

  const onGameOver = useCallback(() => {
    setIsGameOver(true);
  }, []);

  const onGameReset = useCallback(() => {
    setIsGameOver(false);
    resetGame();
  }, [resetGame]);

  return (
    <div className={styles.playingSurface}>
      <div>
        <LayoutHeader>
          <GameHeader />
        </LayoutHeader>
        <Board
          tiles={tiles}
          diceCollection={diceCollection}
          isGameOver={isGameOver}
          letterQueue={letterQueue}
        />
      </div>
      <div>
        <LayoutHeader>
          <NewGameHeader onGameOver={onGameOver} onGameReset={onGameReset} />
        </LayoutHeader>
        <ScoringPanel isGameOver={isGameOver} />
      </div>
    </div>
  );
};
